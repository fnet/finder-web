/*
 * $RCSfile: LoginTemplate.java,v $
 * $Revision: 1.1 $
 * $Date: 2017-04-23 $
 *
 * JSP generated by JspCompiler-1.0.0 (built 2017-04-23 20:20:33 560)
 */
package com.skin.finder.servlet.template;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



/**
 * <p>Title: LoginTemplate</p>
 * <p>Description: </p>
 * @author JspKit
 * @version 1.0
 */
public class LoginTemplate extends com.skin.finder.web.servlet.JspServlet {
    private static final long serialVersionUID = 1L;
    private static final LoginTemplate instance = new LoginTemplate();


    /**
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    public static void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        instance.service(request, response);
    }

    /**
     * @param request
     * @param response
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void service(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("text/html; charset=utf-8");
        PrintWriter out = response.getWriter();

        out.write("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\r\n");
        out.write("<html xmlns=\"http://www.w3.org/1999/xhtml\">\r\n<head>\r\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>\r\n");
        out.write("<meta http-equiv=\"Pragma\" content=\"no-cache\"/>\r\n<meta http-equiv=\"Cache-Control\" content=\"no-cache\"/>\r\n");
        out.write("<meta http-equiv=\"Expires\" content=\"0\"/>\r\n<title>Login</title>\r\n<script type=\"text/javascript\" src=\"");
        this.print(out, request.getAttribute("requestURI"));
        out.write("?action=res&path=/finder/jquery-1.7.2.min.js\"></script>\r\n<script type=\"text/javascript\">\r\n");
        out.write("<!--\r\njQuery(function() {\r\n    jQuery(\"#submit\").click(function() {\r\n        var userName = jQuery.trim(jQuery(\"#s1\").val());\r\n");
        out.write("        var password = jQuery.trim(jQuery(\"#s2\").val());\r\n        var params = \"userName=\" + encodeURIComponent(userName) + \"&password=\" + encodeURIComponent(password);\r\n");
        out.write("        var requestURI = window.location.pathname;\r\n\r\n        jQuery.ajax({\r\n            type: \"post\",\r\n");
        out.write("            url: requestURI + \"?action=finder.login\",\r\n            dataType: \"json\",\r\n");
        out.write("            data: params,\r\n            error: function(req, status, error) {\r\n                alert(\"系统错误，请稍后再试！\");\r\n");
        out.write("            },\r\n            success: function(result) {\r\n                if(result.status == 200) {\r\n");
        out.write("                    window.location.href = \"");
        this.print(out, request.getAttribute("contextPath"));
        out.write("/index.html\";\r\n                }\r\n                else {\r\n                    alert(result.message);\r\n");
        out.write("                }\r\n            }\r\n        });\r\n    });\r\n});\r\n//-->\r\n</script>\r\n</head>\r\n");
        out.write("<body>\r\n<div style=\"margin: 0px auto 0px auto; width: 600px;\">\r\n    <h3>User Login</h3>\r\n");
        out.write("    <table>\r\n        <tr>\r\n            <td style=\"height: 32px;\"><input id=\"s1\" type=\"text\" style=\"width: 196px; height: 24px;\" placeholder=\"UserName\" value=\"\"/></td>\r\n");
        out.write("        </tr>\r\n        <tr>\r\n            <td style=\"height: 32px;\"><input id=\"s2\" type=\"password\" style=\"width: 196px; height: 24px;\" placeholder=\"Password\" value=\"\"/></td>\r\n");
        out.write("        </tr>\r\n        <tr>\r\n            <td style=\"height: 32px;\"><input id=\"submit\" type=\"button\" style=\"width: 200px; height: 40px;\" value=\"login\"/></td>\r\n");
        out.write("        </tr>\r\n    </table>\r\n</div>\r\n</body>\r\n</html>\r\n");

        out.flush();
    }


}
