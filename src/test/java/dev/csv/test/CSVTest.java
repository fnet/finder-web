package dev.csv.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

import dev.csv.CSVReader;
import dev.csv.DataSet;

/**
 * @author weixian
 * @version 1.0
 */
public class CSVTest {

    /**
     * @param args
     */
    public static void main(String[] args) {

    }

    /**
     * @param resource
     */
    public void test1(String resource) {
        CSVReader csvReader = null;
        InputStream inputStream = CSVTest.class.getResourceAsStream(resource);

        try {
            csvReader = new CSVReader(inputStream, "utf-8");

            /**
             * header不是必须的
             * 如果没有可以不用读
             */
            List<String> headers = csvReader.next();
            List<String> data = null;

            while((data = csvReader.next()) != null) {
                DataSet dataSet = new DataSet(headers, data);
                long id = dataSet.getLong("id", 0L);
                String field1 = dataSet.getString("field1");
                String field2 = dataSet.getString("field2");
                Date field3 = dataSet.getDate("field3", "yyyy-MM-dd HH:mm:ss");

                System.out.println("\"" + id + "\","
                        + "\"" + field1 + "\","
                        + "\"" + field2 + "\","
                        + "\"" + field3 + "\"");
            }
        }
        catch(IOException e) {
            e.printStackTrace();
        }
        finally {
            if(csvReader != null) {
                csvReader.close();
            }
        }
    }
}
