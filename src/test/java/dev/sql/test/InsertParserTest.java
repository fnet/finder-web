package dev.sql.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import dev.sql.Record;
import dev.sql.parser.InsertParser;

/**
 * @author weixian
 * @version 1.0
 */
public class InsertParserTest {

    /**
     * @param args
     */
    public static void main(String[] args) {

    }

    /**
     * @param input
     */
    public void test1(File input) {
        InputStream inputStream = null;

        try {
            int count = 0;
            inputStream = new FileInputStream(input);
            InsertParser parser = new InsertParser(inputStream, "utf-8");
            Record record = null;

            /**
             * insert语句支持多 values 语法
             */
            while((record = parser.next()) != null) {
                /**
                 * 输出insert语句
                 */
                System.out.println(record.getInsertSql("s%", false));

                this.build(record);
                this.insert();
                count++;
            }
            System.out.println("import: " + count);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if(inputStream != null) {
                try {
                    inputStream.close();
                }
                catch (IOException e) {
                }
            }
        }
    }

    /**
     * 
     */
    public void insert(/* MyDO myDO*/) {
        /**
         * save to db
         */
    }

    /**
     * @param record
     */
    public void /* MyDO */ build(Record record) {
        Long id = record.getLong("id");
        String userName = record.getString("userName");
        System.out.println(id + ", " + userName);
    }
}
