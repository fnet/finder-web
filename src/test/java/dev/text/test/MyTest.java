package dev.text.test;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;

/**
 * @author weixian
 * @version 1.0
 */
public class MyTest {
    Object dispatchServlet;

    /**
     * @version 1.0
     */
    public static class BootstrapClassLoader extends URLClassLoader {
        /**
         * @param urls
         */
        public BootstrapClassLoader(URL[] urls) {
            super(urls);
        }

        /**
         * @param urls
         * @param parent
         */
        public BootstrapClassLoader(URL[] urls, ClassLoader parent) {
            super(urls, parent);
        }

        /**
         * @param parent
         * @param lib
         * @return HotBootClassLoader
         */
        public static BootstrapClassLoader getClassLoader(final ClassLoader parent, final File lib) {
            URL[] urls = getRepositories(lib.listFiles());
            return getClassLoader(parent, urls);
        }

        /**
         * @param parent
         * @param repositories
         * @return HotBootClassLoader
         */
        public static BootstrapClassLoader getClassLoader(final ClassLoader parent, final URL[] repositories) {
            ClassLoader classLoader = AccessController.doPrivileged(new PrivilegedAction<ClassLoader>() {
                @Override
                public ClassLoader run() {
                    if(parent == null) {
                        return new BootstrapClassLoader(repositories);
                    }
                    else {
                        return new BootstrapClassLoader(repositories, parent);
                    }
                }
            });
            return (BootstrapClassLoader)classLoader;
        }

        /**
         * @param parent
         * @param repositories
         * @return ClassLoader
         */
        public static ClassLoader getClassLoader(final ClassLoader parent, final List<URL> repositories) {
            URL[] urls = new URL[repositories.size()];
            repositories.toArray(urls);
            return getClassLoader(parent, urls);
        }

        /**
         * @param files
         * @return URL[]
         */
        public static URL[] getRepositories(File[] files) {
            Set<URL> set = new LinkedHashSet<URL>();

            if(files != null) {
                for(File file : files) {
                    String fileName = file.getName().toLowerCase(Locale.ENGLISH);

                    if(fileName.endsWith(".jar") || fileName.endsWith(".zip")) {
                        try {
                            set.add(file.toURI().toURL());
                        }
                        catch(IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            URL[] urls = new URL[set.size()];
            set.toArray(urls);
            return urls;
        }

        /**
         * @param urls
         * @param files
         * @return URL[]
         */
        public static List<URL> addRepositories(List<URL> urls, File[] files) {
            Set<URL> set = new LinkedHashSet<URL>();

            if(files != null) {
                for(File file : files) {
                    String fileName = file.getName().toLowerCase(Locale.ENGLISH);

                    if(fileName.endsWith(".jar") || fileName.endsWith(".zip")) {
                        try {
                            set.add(file.toURI().toURL());
                        }
                        catch(IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            urls.addAll(set);
            return urls;
        }
    }

    /**
     * @param path
     * @return ClassLoader
     * @throws Exception
     */
    protected ClassLoader getClassLoader(String path) throws Exception {
        List<URL> repositories = new ArrayList<URL>();
        repositories.add(new File(path).toURI().toURL());

        ClassLoader parent = this.getClass().getClassLoader();
        return BootstrapClassLoader.getClassLoader(parent, repositories);
    }

    /**
     * @param classLoader
     * @param className
     * @param parameterTypes
     * @param args
     * @return Object
     * @throws Exception
     */
    protected static Object getInstance(ClassLoader classLoader, String className) throws Exception {
        return classLoader.loadClass(className).newInstance();
    }

    /**
     * @param object
     * @param name
     * @param types
     * @param args
     * @throws Exception
     */
    protected static void invoke(Object object, String name, Class<?>[] types, Object[] args) throws Exception {
        Method method = object.getClass().getMethod(name, types);
        method.invoke(object, args);
    }

    /**
     * @param workspace
     * @param name
     * @param location
     * @throws Exception
     */
    protected static void addWorkspace(ClassLoader classLoader, String name, String location) throws Exception {
        Class<?> type = classLoader.loadClass("dev.text.WorkspaceManager");
        Method method = type.getMethod("add", new Class<?>[]{String.class, String.class});
        method.invoke(null, new Object[]{name, location});
    }

    /**
     * @param request
     * @param response
     */
    protected void reload(HttpServletRequest request, HttpServletResponse response) {
        if(this.dispatchServlet != null) {
            try {
                invoke(this.dispatchServlet, "destroy", null, null);
            }
            catch (Exception e) {
            }
            this.dispatchServlet = null;
        }
        response.setStatus(304);
        response.setHeader("Location", request.getRequestURI());
    }

    /**
     * @return boolean
     */
    protected static boolean getTrue() {
        return true;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
    }

    /**
     * @throws ServletException 
     * @throws IOException 
     */
    public void jspService() throws ServletException, IOException {
        ServletContext application = this.getServletContext();
        HttpServletRequest request = this.getRequest();
        HttpServletResponse response = this.getResponse();
        PageContext pageContext = this.getPageContext();
        JspWriter out = pageContext.getOut();
        String action = request.getParameter("action");

        if(action != null && action.equals("reload")) {
            this.dispatchServlet = null;
        }

        try {
            if(this.dispatchServlet == null) {
                String jar = application.getRealPath("/WEB-INF/mydata/dev-text-1.0.0.jar");
                ClassLoader classLoader = this.getClassLoader(jar);
                Thread.currentThread().setContextClassLoader(classLoader);
    
                addWorkspace(classLoader, "app", "file:/home/admin/atpc");
                addWorkspace(classLoader, "app", "file:/home/admin/atpc");
    
                this.dispatchServlet = getInstance(classLoader, "dev.web.servlet.DispatchServlet");
                invoke(this.dispatchServlet, "setServletContext", new Class<?>[]{ServletContext.class}, new Object[]{application});
                invoke(this.dispatchServlet, "setPackages", new Class<?>[]{String[].class}, new Object[]{new String[]{
                    "dev.text.servlet",
                    "dev.tools.servlet",
                    "dev.sql.servlet",
                    "dev.demon.servlet"
                }});
                invoke(this.dispatchServlet, "init", new Class<?>[0], new Object[0]);
            }

            invoke(this.dispatchServlet, "service",
                    new Class<?>[]{HttpServletRequest.class, HttpServletResponse.class},
                    new Object[]{request, response});
        }
        catch(Throwable t) {
            throw new ServletException(t);
        }
        finally {
            response.flushBuffer();
            out.clear();
            // out = pageContext.pushBody();
        }

        if(getTrue()) {
            return;
        }
    }

    /**
     * @return PageContext
     */
    private PageContext getPageContext() {
        return null;
    }

    /**
     * @return ServletContext
     */
    public ServletContext getServletContext() {
        return null;
    }

    /**
     * @return HttpServletRequest
     */
    public HttpServletRequest getRequest() {
        return null;
    }

    /**
     * @return HttpServletResponse
     */
    public HttpServletResponse getResponse() {
        return null;
    }
}
