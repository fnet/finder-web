@ECHO OFF
@IF exist "E:\WorkSpace" @SET WORKSPACE=E:\WorkSpace
@IF exist "d:\workspace2" @SET WORKSPACE=d:\workspace2

@ECHO OFF copy finder-web-2.0.0.jar
@SET SOURCE=build\release\finder-web-2.0.0.jar

copy "%SOURCE%" "%WORKSPACE%\webcat\webcat-local\webapp\WEB-INF\lib"
copy "%SOURCE%" "%WORKSPACE%\fmbak\webapp\WEB-INF\lib"
